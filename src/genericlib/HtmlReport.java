package genericlib;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import driverscript.DriverScript;
import driverscript.Xls_Reader;



public class HtmlReport {
	
	public static String GetReportName(String ReportName)
	{		
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();		
			String TempFileName=dateFormat.format(date);
			String NewFileName1=TempFileName.replace("/","_");
			String NewFileName2=NewFileName1.replace(" ","_");
			String NewFileName=NewFileName2.replace(":","_");			
			String ReportFileName =(System.getProperty("user.dir")+"\\src\\Results\\"+ReportName+""+NewFileName+".html");
		    return ReportFileName;
		
	}
	
	
	public static void ExcelResult(String status)
	{
		Xls_Reader xres=new Xls_Reader(System.getProperty("user.dir")+"\\src\\Results\\ExcelResult.xlsx");
		int rowcount=xres.getRowCount(DriverScript.vAppName);
		xres.setCellData(DriverScript.vAppName, "ProjectName", rowcount+1, DriverScript.vAppName);
		xres.setCellData(DriverScript.vAppName, "ModuleName", rowcount+1, DriverScript.vModuleName);
		xres.setCellData(DriverScript.vAppName, "TestCaseName", rowcount+1, DriverScript.vTCName);
		xres.setCellData(DriverScript.vAppName, "TestCaseDescription", rowcount+1, DriverScript.vDescription);
		xres.setCellData(DriverScript.vAppName, "Status", rowcount+1, status);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();		
		String TempFileName=dateFormat.format(date);		
		xres.setCellData("vTiger", "DateTime", rowcount+1, TempFileName);
	}

}
